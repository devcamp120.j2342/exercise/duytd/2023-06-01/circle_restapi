package com.devcamp.circle_restapi.controllers;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circle_restapi.models.Circle;

@RestController
@RequestMapping("/")
@CrossOrigin
public class circlecontroller {
    
    //khai bao api dang get data
    @GetMapping("/circle-area")
    public double getCircleArea(@RequestParam("radius") double radius){

        Circle circle = new Circle(5.0);
        System.out.println(circle.toString());
        return circle.getArea();
    }
}
